---
title: Darcs Buch
description: Für alle die schon immer mal wissen wollten, wie man, wenn man mit Textdateien arbeitet, die Änderungen, die man macht, verwalten und im Auge behalten kann, und für alle Experten, die von git frustriert sind, bietet darcs eine interessante Alternative. Unser Buch erklärt dir, wie du damit am besten umgehst. Aktuell nur auf Englisch. Eine deutsche Version ist in arbeit. 
url: https://darcsbook.acmelabs.space
---
