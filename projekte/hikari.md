---
title: Hikari
description: hikari ist ein stacking Window Manager mit zusätzlicher tiling Funktionalität und wurde stark vom Calm Window manager (cwm(1)) inspiriert.
url: https://hikari.acmelabs.space
---
