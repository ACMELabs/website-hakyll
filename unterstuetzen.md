---
title: Unterstützen
---


ACME Labs e.V. ist ein eingetragener gemeinnütziger Verein der komplett aus
Mitgliedsbeiträgen und Spenden getragen wird. Unterstützen kannst du uns auf
verschiedenen Wegen.

## Taten
Du kennst dich mit einem Thema aus und willst es anderen Vermitteln? Warum
keinen Vortrag oder einen Workshop in unserem Vortragsraum halten?

Unterstütze andere bei Ihrem Projekt - es gibt immer was zu tun. Komm bei uns
vorbei und frag nach.

## Sachspenden
Du hast alte Technik die du nicht mehr brauchst, aber für uns vielleicht
nützlich seinen könnte? 

Sachspenden sind **immer** im Voraus abzusprechen. Wende dich dazu bitte
an [`vorstand@acmelabs.space`](mailto:vorstand@acmelabs.space) .

## Geldspenden

#### Spendenkonto
IBAN: DE29 4805 0161 0000 1362 42

BIC: SPBIDE3BXXX

#### PayPal
Auf [PayPal.me/AcmeLabsEV](https://paypal.me/acmelabsev) kannst du uns
spenden per PayPal zukommen lassen.

#### Spendennachweis
Wenn du einen Spendennachweis brauchst, wende dich bitte an
[`vorstand@acmelabs.space`](mailto:vorstand@acmelabs.space)
