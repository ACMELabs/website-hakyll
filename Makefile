.PHONY: build rebuild clean compile watch upload blog

build:  compile
	cabal run site build

rebuild: compile
	cabal run site rebuild

upload:
	rsync -vrb --backup-dir ../website-backup --checksum --delete _site/ acmelabs.space:/usr/jails/www/usr/local/www/acmelabs/

beta:
	rsync -vrb --checksum --delete _site/ acmelabs.space:/usr/jails/www/usr/local/www/beta/

updiff:
	rsync -vrn --checksum _site/ acmelabs.space:/usr/jails/www/usr/local/www/acmelabs/

clean:
	rm -rf _site
	rm -rf _cache

clean-all: clean
	rm -rf projekte/darcs-book
	rm -rf blog

watch: compile
	cabal run site watch

compile: blog
	cabal build 
