-----
title: FLINTA treffen
date: Jeden 1. und 3. Dienstag im Monat ab 19Uhr
-----

Offenes Treffen und Safespace für FLINTA-Mensch en zur

* Vernetzung
* Wissensaustausch
* gemütlich zusammensitzen

Der CoC der ACME Labs gilt auch für diese Veranstaltung.

An diesen Abende sind die ACME Labs nur für Besuchende des FLINTA-Treffen geöffnet.
