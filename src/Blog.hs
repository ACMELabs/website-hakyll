{-# LANGUAGE OverloadedStrings #-}
module Blog (blog) where

import           Control.Monad
import           Data.Monoid
import           Data.Time.Format
import           Data.Time.Clock
import           Hakyll

blog :: Rules ()
blog = do
  create ["blog.html"] $ do
    route $ idRoute
    compile $ do
      posts <- loadAllSnapshots "blog/*.md" "content"
      let blogCtx =
            listField "posts" (teaserField "teaser" "content" <>
                               dateField "date" "%Y-%m-%d" <>
                               postCtx) (return posts) <>
            constField "title" "Blog" <>
            defaultContext

      makeItem ""
        >>= loadAndApplyTemplate "templates/blog.html" blogCtx
        >>= loadAndApplyTemplate "templates/default.html" blogCtx
        >>= relativizeUrls

  match "blog/*.md" $ do
    route $ setExtension "html"
    compile $ do
      pandocCompiler
      >>= saveSnapshot "content"
      >>= loadAndApplyTemplate "templates/blog_post.html" postCtx
      >>= loadAndApplyTemplate "templates/default.html" postCtx
      >>= relativizeUrls

  match "blog/images/*" $ do
    compile copyFileCompiler

  create ["atom.xml"] $ do
    route idRoute
    compile (feedCompiler renderAtom)

  create ["rss.xml"] $ do
    route idRoute
    compile (feedCompiler renderRss)

type FeedRenderer = FeedConfiguration
                    -> Context String
                    -> [Item String]
                    -> Compiler (Item String)
feedCompiler :: FeedRenderer -> Compiler(Item String)
feedCompiler renderer =
  renderer blogFeedConfiguration feedCtx
    =<< fmap (take 10) . recentFirst
    =<< loadAllSnapshots "blog/*.md" "content"

feedCtx :: Context String
feedCtx = updatedField <> postCtx <> bodyField "description"

postCtx :: Context String
postCtx =
  dateField "date" "%B %e, %Y" <>
  defaultContext

blogFeedConfiguration :: FeedConfiguration
blogFeedConfiguration = FeedConfiguration
  { feedTitle = "ACME Labs Blog"
  , feedDescription = "Verein Aktiviteaten und Events des ACME Labs e.V."
  , feedAuthorName = "ACME Labs e.V."
  , feedAuthorEmail = "sm@acmelabs.space"
  , feedRoot = "https://acmelabs.space"
  }

updatedField :: Context String
updatedField = field "updated" $ \i -> do
    let locale = defaultTimeLocale
    time <- getUpdatedUTC locale $ itemIdentifier i
    return $ formatTime locale "%Y-%m-%dT%H:%M:%SZ" time


getUpdatedUTC :: (MonadMetadata m, MonadFail m) => TimeLocale -> Identifier -> m UTCTime
getUpdatedUTC locale id' = do
    metadata <- getMetadata id'
    let tryField k fmt = lookupString k metadata >>= parseTime' fmt
    maybe empty' return $ msum [tryField "updated" fmt | fmt <- formats]
  where
    empty'     = fail $ "Hakyll.Web.Template.Context.getUpdatedUTC: " ++ "could not parse time for " ++ show id'
    parseTime' = parseTimeM True locale
    formats    =
        [ "%a, %d %b %Y %H:%M:%S %Z"
        , "%Y-%m-%dT%H:%M:%S%Z"
        , "%Y-%m-%d %H:%M:%S%Z"
        , "%Y-%m-%d"
        , "%B %e, %Y %l:%M %p"
        , "%B %e, %Y"
        , "%b %d, %Y"
        ]
