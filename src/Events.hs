{-# LANGUAGE OverloadedStrings #-}
module Events (events, nextEvent) where

import           Control.Monad
import           Data.Monoid ((<>), mempty)
import           Data.Time.Format
import           Data.Time.Clock
import           Hakyll
import           Data.Binary  (Binary)
import           Data.Typeable (Typeable)

events :: Rules ()
events = do
  match "events.md" $ do
    route $ setExtension "html"
    time <- preprocess getCurrentTime
    compile $ do
      events <- notInPast time
        =<< chronological
        =<< loadAllSnapshots (fromRegex "events/[0-9].*\\.md") "content"
      meetings <- loadAllSnapshots (fromRegex "events/[a-zA-Z].*\\.md") "content"
      let eventCtx =
               (if   null events
                then mempty 
                else listField "events" eventsCtx (return events)
               )
            <> (if   null meetings 
                then mempty 
                else listField "meetings" eventsCtx (return meetings)
               )
            <> defaultContext

      pandocCompiler
        >>= loadAndApplyTemplate "templates/events-current.html" eventCtx
        >>= loadAndApplyTemplate "templates/default.html" eventCtx
        >>= relativizeUrls

  match "event-archive.md" $ do
    route $ setExtension "html"
    time <- preprocess getCurrentTime
    compile $ do
      events <- notInFuture time 
        =<< return . reverse
        =<< chronological 
        =<< loadAllSnapshots (fromRegex "events/[0-9].*\\.md") "content"
      let eventCtx =
            listField "events" eventsCtx (return events) <>
            defaultContext

      pandocCompiler
        >>= loadAndApplyTemplate "templates/events-past.html" eventCtx
        >>= loadAndApplyTemplate "templates/default.html" eventCtx
        >>= relativizeUrls


  match "events/*.md" $ do
    route $ setExtension "html"
    compile $ do
      pandocCompiler
      >>= saveSnapshot "content"
      >>= loadAndApplyTemplate "templates/blog_post.html" eventsCtx
      >>= loadAndApplyTemplate "templates/default.html" eventsCtx
      >>= relativizeUrls


eventsCtx :: Context String
eventsCtx =
  teaserField "teaser" "content" <>
  dateField "date" "%Y-%m-%d %H:%M" <>
  defaultContext

notInPast :: (MonadMetadata m, MonadFail m) => UTCTime -> [Item a] -> m [Item a]
notInPast time =
  filterM (\a -> do
  itemTime <- getItemUTC defaultTimeLocale (itemIdentifier a)
  return (time < itemTime))

notInFuture :: (MonadMetadata m, MonadFail m) => UTCTime -> [Item a] -> m [Item a]
notInFuture time =
  filterM (\a -> do
  itemTime <- getItemUTC defaultTimeLocale (itemIdentifier a)
  return (time > itemTime))

nextEvent :: (Binary a, Typeable a) => UTCTime -> Compiler [Item a]
nextEvent time =
  fmap (take 1) $ notInPast time =<< chronological =<< loadAllSnapshots (fromRegex "events/\\d.*\\.md") "content"
