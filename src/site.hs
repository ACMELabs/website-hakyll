--------------------------------------------------------------------------------
{-# LANGUAGE OverloadedStrings #-}
import           Hakyll

import           Text.Pandoc.Options
--import           Text.Regex

import           Data.Monoid
import           Data.Time
import           System.IO

--import           Blog
import           Color
import           Events
--------------------------------------------------------------------------------
main :: IO ()
main = do
  hakyll $ do
    -- blog
    events
    match "images/*" $ do
        route   idRoute
        compile copyFileCompiler

    create ["css/style.css"] $ do
        route idRoute
        let buttonColors = makeCss acmeColors 6
        compile $ do
          css <- getResourceString
          withItemBody (return . compressCss . (++ buttonColors)) css

    create ["projekte.html"] $ do
      route idRoute
      compile $ do
        projects <- loadAll "projekte/*"
        let projectsCtx =
              listField "projects" defaultContext (return projects) <>
              constField "pagetitle" "Projekte" <>
              constField "title" "Projekte" <>
              defaultContext
        makeItem ""
         >>= loadAndApplyTemplate "templates/projects-list.html" projectsCtx
         >>= loadAndApplyTemplate "templates/default.html" projectsCtx
         >>= relativizeUrls

    match "projekte/*" $ do
      route $ setExtension "html"
      compile $ pandocCompiler
          >>= loadAndApplyTemplate "templates/default.html" defaultContext
          >>= relativizeUrls

    match "index.md" $ do
        route $ setExtension "html"
        time <- preprocess getCurrentTime
        compile $ do
          next <- nextEvent time
          let indexCtx =
                listField
                  "nextEvent"
                  (dateField "date" "%Y-%m-%d %H:%M" <> defaultContext)
                  (return next)
                <> defaultContext
          pandocCompiler
            >>= loadAndApplyTemplate "templates/index.html" indexCtx
            >>= loadAndApplyTemplate "templates/default.html" indexCtx
            >>= relativizeUrls

    match (fromList ["datenschutz.md", "impressum.md", "kontakt.md", "coc.md", "unterstuetzen.md"]) $ do
        route   $ setExtension "html"
        compile $ pandocCompiler
            >>= loadAndApplyTemplate "templates/default.html" defaultContext
            >>= relativizeUrls

    match "templates/*" $ compile templateBodyCompiler

    create ["favicon.ico"] $ do
      route idRoute
      compile $ makeItem $ Redirect "/images/favicon.ico"

    create ["apple-touch-icon.png"] $ do
      route idRoute
      compile $ makeItem $ Redirect "/images/apple-touch.icon.png"

    match (fromList ["robots.txt"]) $ do
        route   idRoute
        compile copyFileCompiler
