---
title: A Collective Making Everything
description: Ein Hackspace in Bielefeld
---

::::: {.definition}
**Hack** (\[hæk\]; englisch für technischer Kniff): Funktionserweiterung
oder Problemlösung die ein Ziel auf eine ungewöhnliche Weise erreicht;
oft im Kontext einer Zweckentfremdung.
:::::

Wir sind ein Hack- bzw. Makespace in Bielefeld und
interessieren uns in erster Linie für Technologie und wie diese unsere
Gesellschaft verändert. Wir wissen, dass Technologie alleine nicht die
Welt retten und auch missbräuchlich genutzt werden kann, sind aber davon
überzeugt, dass wir Werkzeuge bauen können die dabei helfen, eine
bessere Zukunft zu gestalten. Bei uns ist jede Person willkommen, die
Interesse an unseren Themen hat, unabhängig von den persönlichen
Fähigkeiten.

„Be excellent to each other“ ist einfacher gesagt als getan, und jede*r
versteht da etwas anderes drunter. Wenn du wissen möchtest, was wir
unter einem vernünftigen Miteinander verstehen, dann lies unseren [Code
of Conduct](coc.html).
